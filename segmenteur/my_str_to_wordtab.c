/*
** my_str_to_wordtab.c for my_str_to_wordtab in /home/loisel_k/Librairie
** 
** Made by loisel_k
** Login   <loisel_k@epitech.net>
** 
** Started on  Wed Oct 23 01:36:01 2013 loisel_k
** Last update Wed Nov  6 22:31:25 2013 kevin loiseleur
*/

#include <stdio.h>
#include <stdlib.h>

char	is_letter_print(char t)
{
  if ((t >= 'a' && t <= 'z') ||
      (t >= 'A' && t <= 'Z') ||
      (t >= '0' && t <= '9'))
    return ('t');
  return ('f');
}

int		create_tab(char *str)
{
  int		count;
  int		i;

  i = 0;
  count = 0;
  while (str && str[i])
    {
      if (is_letter_print(str[i]) == 't')
	{
	  count++;
	  while (str[i] && (is_letter_print(str[i]) == 't'))
	    i++;
	}
      i++;
    }
  return (count);
}

int		create_str(int i, char *str)
{
  int		len;

  len = 0;
  while (str && str[i])
    {
      if (str[i] && (is_letter_print(str[i]) == 't'))
	len++;
      i++;
    }
  return (len);
}

char		**my_str_to_wordtab(char *str)
{
  char		**tab;
  int		i1;
  int		i;
  int		j;
  char		*tmp;

  j = 0;
  i = 0;0
  if ((tab = malloc(create_tab(str) * sizeof(*tab))) == NULL)
    return (NULL);
  while (j < create_tab(str))
    {
      i1 = 0;
      while (is_letter_print(str[i]) == 'f')
	i++;
      if ((tmp = malloc(create_str(i, str) * sizeof(tmp))) == NULL)
	return (NULL);
      while (is_letter_print(str[i]) == 't')
	  tmp[i1++] = str[i++];
      tmp[i1] = '\0';
      tab[j] = tmp;
      j++;
    }
  tab[j] = NULL;
  return (tab);
}

void		my_show_wordtab(char **tab)
{
  int		i;

  i = 0;
  while (tab[i] != NULL)
    printf("%s\n", tab[i++]);
  return ;
}

int		main(int ac, char **av)
{
  my_show_wordtab(my_str_to_wordtab(av[1]));
}
