/*
** tree.h for tree.h in /home/loisel_k/travail/Librairie/tree/test
** 
** Made by kevin loiseleur
** Login   <loisel_k@epitech.net>
** 
** Started on  Sat Nov  2 13:15:05 2013 kevin loiseleur
** Last update Sat Nov  2 17:10:21 2013 kevin loiseleur
*/

#ifndef TREE_H_
# define TREE_H_

typedef struct		node
{
  unsigned int		key;
  struct node		*left;
  struct node		*right;
}			node;

#endif /* TREE_H_ */
