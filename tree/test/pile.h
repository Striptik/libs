/*
** tree.c for main function for tree in /home/loisel_k/travail/Librairie/tree/test
** 
** Made by kevin loiseleur
** Login   <loisel_k@epitech.net>
** 
** Started on  Sat Nov  2 13:21:29 2013 kevin loiseleur
** Last update Sat Nov  2 17:10:18 2013 kevin loiseleur
*/

typedef struct		pile
{
  void			*data;
  struct pile		*prev;
}			pile;

pile			*empiler(pile *ptr, void *data)
{
  pile			*ptr2;
  
  if ((ptr2 = malloc(sizeof(pile))) == NULL)
    return (NULL);
  ptr2->data = data;
  ptr2->prev = ptr;
  return (ptr2);
}

pile			*depiler(pile *ptr, void **data)
{
  pile			*ptr2;

  *data = ptr->data;
  free(ptr);
  return (ptr2);
}

