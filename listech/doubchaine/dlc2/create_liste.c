/*
** create_liste.c for create_list in /home/loisel_k/travail/Librairie/listech/doubchaine/dlc2
** 
** Made by kevin loiseleur
** Login   <loisel_k@epitech.net>
** 
** Started on  Sat Nov 30 01:53:33 2013 kevin loiseleur
** Last update Sat Nov 30 02:17:00 2013 kevin loiseleur
*/

#include <stdlib.h>
#include "dlc2.h"

t_dlc			*create_list(const void *element)
{
  t_dlc			*link;

  if (!(link = malloc(sizeof(*link))))
    return (NULL);
  link->data = element;
  link->previous = NULL;
  link->next = NULL;
  return (link);
}
