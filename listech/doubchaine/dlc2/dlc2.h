/*
** dlc2.h for double liste chainee V2 Header in /home/loisel_k/travail/Librairie/listech/doubchaine/dlc2
** 
** Made by kevin loiseleur
** Login   <loisel_k@epitech.net>
** 
** Started on  Sat Nov 30 01:32:54 2013 kevin loiseleur
** Last update Sat Nov 30 02:21:58 2013 kevin loiseleur
*/

#ifndef MY_LIST_H_
# define MY_LIST_H_

typedef struct		s_dlc
{
  const void		*data;
  struct s_list		*prev;
  struct s_list		*next;
}			t_dlc;

typedef struct		s_pos
{
  t_dlc			*beg;
  t_dlc			*end;
  int			size;
}			t_pos;

# define BEG		0
# define END		1

/*
#ifndef FSAME
# define FSAME

typedef int (*fsame)(const void *)(const void *);

#endif

#ifndef FMAP
# define FMAP

typedef void (*fmap)(void *);

#endif

#ifndef FPRINT
# define FPRINT

typedef void (*fprint)(const void *)

#endif
*/

/*
** Creation
*/

t_dlc			*create_list(const void *);

/*
** Insertion
*/

int			insert_list(t_dlc **element, int pos)

#endif
