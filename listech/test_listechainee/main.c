/*
** main.c for my_put_in_list in /home/loisel_k/rendu/Piscine-C-Jour_12/test
** 
** Made by loisel_k
** Login   <loisel_k@epitech.net>
** 
** Started on  Wed Oct 23 09:56:12 2013 loisel_k
** Last update Wed Oct 23 12:42:56 2013 loisel_k
*/

#include <stdlib.h>
#include "list.h"

int		main(int ac, char **av)
{
  struct s_list	*list;

  list = NULL;
  my_put_in_list(&list, "toto", 42);
  my_put_in_list(&list, "tata", 24);
  my_show_list(list);
  return (0);
}
