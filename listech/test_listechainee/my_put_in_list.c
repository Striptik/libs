/*
** my_put_in_list.c for my_put_in_list in /home/loisel_k/rendu/Piscine-C-Jour_12/test
** 
** Made by loisel_k
** Login   <loisel_k@epitech.net>
** 
** Started on  Wed Oct 23 09:27:21 2013 loisel_k
** Last update Wed Oct 23 11:18:06 2013 loisel_k
*/

// 2 avantages : pouvoir ajouter un element ou on veut
// espace pas forcement contigu puisque pointeur vers le prochain maillon donc pratique quand beaucoup d'espace

#include <stdlib.h>
#include "list.h"

int		my_put_in_list(struct s_list **list, char *name, int age)
{
  struct s_list	*elem;

  elem = malloc(sizeof(*elem));
  if (elem == NULL)
    return (1);
  elem->name = name;
  elem->age = age;
  elem->next = *list;
  *list = elem;

  return (0);
}

void		my_show_list(struct s_list *list)
{
  struct s_list *tmp;

  tmp = list;
  while (tmp != NULL)
    {
      my_putstr(tmp->name);
      my_putchar('\n');
      tmp = tmp->next;
    }
}
