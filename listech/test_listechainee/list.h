/*
** list.h for list.h in /home/loisel_k/rendu/Piscine-C-Jour_12/test
** 
** Made by loisel_k
** Login   <loisel_k@epitech.net>
** 
** Started on  Wed Oct 23 09:29:11 2013 loisel_k
** Last update Wed Oct 23 10:02:59 2013 loisel_k
*/

#ifndef LIST_H_
# define LIST_H_

struct		s_list
{
  char		*name;
  int		age;
  struct s_list	*next;

};

int		my_put_in_list(struct s_list **list, char *name, int age);

#endif /* LIST_H_ */
